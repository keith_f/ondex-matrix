/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 12-Nov-2012, 11:31:29
 */

package uk.ac.ncl.ondexmatrix;

import java.util.Arrays;
import java.util.Set;
import net.sourceforge.ondex.core.ONDEXConcept;
import net.sourceforge.ondex.core.ONDEXGraph;
import net.sourceforge.ondex.core.ONDEXRelation;
import net.sourceforge.ondex.core.RelationType;

/**
 *
 * @author Keith Flanagan
 */
public class MatrixCreator
    implements Worker
{
  private static final char NEW_LINE = '\n';
  private static final char SEPARATOR = '\t';
  private static final String NO_RELATION = ".";
  
  private final ONDEXGraph graph;
  private final RelationType relType;
  private Appendable output;
 
  public MatrixCreator(ONDEXGraph graph, RelationType relType)
  {
    this.graph = graph;
    this.relType = relType;
  }
  
  @Override
  public void setOutput(Appendable output)
  {
    this.output = output;
  }

  @Override
  public void run()
  {
    try
    {
      /*
       * Create a sorted list of numerical concept IDs.
       */
      final int[] conceptIds = new int[graph.getConcepts().size()];
      int idx = 0;
      for (ONDEXConcept c : graph.getConcepts()) {
        conceptIds[idx++] = c.getId();
      }
      Arrays.sort(conceptIds);

      /*
       * First line contains the 'X' concept IDs.
       */
      output.append(SEPARATOR);
      for (int cId : conceptIds) {
        output.append(String.valueOf(cId));
        if (cId != conceptIds[conceptIds.length-1]) {
          output.append(SEPARATOR);
        } else {
          output.append(NEW_LINE);
        }
      }

      for (int yId : conceptIds) {
        ONDEXConcept yC = graph.getConcept(yId);
        output.append(String.valueOf(yC.getId())).append(SEPARATOR);
        for (int xId : conceptIds) {
          ONDEXConcept xC = graph.getConcept(xId);
          ONDEXRelation relation = graph.getRelation(yC, xC, relType);
          
          if (relation == null) {
            output.append(NO_RELATION);
          } else {
            output.append(String.valueOf(relation.getId()));
          }
          output.append(SEPARATOR);
        }
        output.append(NEW_LINE);
      }


    }
    catch(Exception e) {
      throw new RuntimeException(
          "Failed to create matrix for relation type: "+relType, e);
    }
  }
}
