/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 10-Jul-2012, 15:28:02
 */
package uk.ac.ncl.ondexmatrix;


import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import net.sourceforge.ondex.annotations.Authors;
import net.sourceforge.ondex.annotations.Custodians;
import net.sourceforge.ondex.annotations.Status;
import net.sourceforge.ondex.annotations.StatusType;
import net.sourceforge.ondex.args.ArgumentDefinition;
import net.sourceforge.ondex.args.FileArgumentDefinition;
import net.sourceforge.ondex.args.StringArgumentDefinition;
import net.sourceforge.ondex.core.*;
import net.sourceforge.ondex.parser.ONDEXParser;


/**
 *
 * @author Keith Flanagan
 */
@Authors(authors = { "Keith Flanagan" }, emails = { "keith.flanagan@ncl.ac.uk" })
@Custodians(custodians = { "Keith Flanagan" }, emails = { "keith.flanagan@ncl.ac.uk" })
@Status(status = StatusType.STABLE, 
    description = "For a given graph, this plugin creates a number of matrices "
    + "(one for each relation type, plus an additional summary matrix) that, "
    + "taken together, describe the relatedness of every concept instance with "
    + "every other concept instance in the graph. \n"
    + "Each matrix is of concept vs concept for a particular relation type, and "
    + "the matrix cell content is the ONDEX ID of the relation instance between "
    + "those concepts (or empty if no relation exists). \n"
    + "Optionally, this plugin can also create a summary matrix, where the rows "
    + "and columns represent concept instances (as before), but the cell "
    + "content is a boolean value that is TRUE if any relation exists between "
    + "a concept, and FALSE if no relation exists between a concept.")
public class GraphToMatrixPlugin
    extends ONDEXParser
{
  private static final Logger logger =
      Logger.getLogger(GraphToMatrixPlugin.class.getName());


  @Override
  public String getId()
  {
    return GraphToMatrixPlugin.class.getSimpleName();
  }

  @Override
  public String getName()
  {
    return "Graph to matrix plugin";
  }

  @Override
  public String getVersion()
  {
    return "1.0";
  }

  @Override
  public String[] requiresValidators()
  {
    return new String[0];
  }

  @Override
  public boolean requiresIndexedGraph()
  {
    return false;
  }
  
  

  @Override
  public ArgumentDefinition<?>[] getArgumentDefinitions()
  {
    return new ArgumentDefinition<?>[] {
      new FileArgumentDefinition("Output directory", 
        "Directory where output files will be created", true, false, false)
    };
  }

  @Override
  public void start()
      throws Exception
  {
    
    try
    {
      double startTime = System.currentTimeMillis();

      logger.info("Argument list: "+Arrays.asList(args));
      logger.info("Argument options: "+getArguments().getOptions());
      String outputDirname = (String) getArguments().getObjectValueArray("Output directory")[0];
      File outputDir = new File(outputDirname);
      
      ScheduledThreadPoolExecutor exe = new ScheduledThreadPoolExecutor(Runtime.getRuntime().availableProcessors());
      
      exe.execute(new WorkerWrapper(new ConceptToTypeListWriter(graph), outputDir, "_concept-list.csv.gz"));
      exe.execute(new WorkerWrapper(new RelationToTypeListWriter(graph), outputDir, "_relation-list.csv.gz"));
      
      /*
       * Schedule worker threads for each relation type
       */
      Set<RelationType> rTypes = graph.getMetaData().getRelationTypes();
      for (RelationType rType : rTypes) {
        logger.info("Scheduling matrix creation task for relation type: "
            + rType.getId() + ", " + rType.getFullname());
        String filename = rType.getId() + ".csv.gz";
        filename = filename.replaceAll(" ", ""); //Remove spaces
        exe.execute(new WorkerWrapper(
            new MatrixCreator(graph, rType), outputDir, filename));
      }
      
      //Attempt shut down and block here until all threads have finished
      exe.shutdown();
      boolean finished = false;
      while (!finished) {
        try {
          finished = exe.awaitTermination(5000, TimeUnit.MILLISECONDS);
        }
        catch(InterruptedException e) {
          e.printStackTrace();
          //Nothing to do here except wait again.
        }
      }
      
      double endTime = System.currentTimeMillis();
      double durationSec = (endTime - startTime) / 1000;
      Runtime runtime = Runtime.getRuntime();
      logger.info("Duration of data population: "+durationSec
          + "\nMemory info: Total: "+(runtime.totalMemory() / 1024)
          + "kb, Max: "+(runtime.maxMemory() / 1024)
          + "kb, Free: "+(runtime.freeMemory() / 1024) + "kb");
    }
    catch(Exception e) {
      e.printStackTrace();
      throw new GraphToPluginException("Something failed while running the plugin", e);
    }
  }

}
