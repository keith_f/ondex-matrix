/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 12-Nov-2012, 12:15:19
 */

package uk.ac.ncl.ondexmatrix;

import java.io.*;
import java.util.zip.GZIPOutputStream;

/**
 * A simple wrapper around <code>Worker</code> implementations for 
 * managing file-handling operations.
 * 
 * @author Keith Flanagan
 */
public class WorkerWrapper
    implements Runnable
{
  private final Worker worker;
  private final File outputFile;
  
  public WorkerWrapper(Worker worker, File dir, String filename)
  {
    this.worker = worker;
    this.outputFile = new File(dir, filename);
  }
  
  @Override
  public void run()
  {
    try {
      //FileWriter fw = new FileWriter(outputFile);
      
      Writer fw = new OutputStreamWriter(
          new GZIPOutputStream(new FileOutputStream(outputFile)));
      
      worker.setOutput(fw);
      worker.run();
      fw.flush();
      fw.close();
    }
    catch(Exception e) {
      e.printStackTrace();
      throw new RuntimeException("Failed to execute worker: "+worker.getClass(), e);
    }
  }

}
