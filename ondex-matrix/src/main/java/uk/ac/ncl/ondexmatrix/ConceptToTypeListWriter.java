/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 12-Nov-2012, 11:31:29
 */

package uk.ac.ncl.ondexmatrix;

import java.util.Arrays;
import java.util.Set;
import net.sourceforge.ondex.core.ONDEXConcept;
import net.sourceforge.ondex.core.ONDEXGraph;
import net.sourceforge.ondex.core.ONDEXRelation;
import net.sourceforge.ondex.core.RelationType;

/**
 *
 * @author Keith Flanagan
 */
public class ConceptToTypeListWriter
    implements Worker
{
  private static final char NEW_LINE = '\n';
  private static final char SEPARATOR = '\t';
  private static final String NO_RELATION = ".";
  
  private final ONDEXGraph graph;
  private Appendable output;
 
  public ConceptToTypeListWriter(ONDEXGraph graph)
  {
    this.graph = graph;
  }
  
  @Override
  public void setOutput(Appendable output)
  {
    this.output = output;
  }

  @Override
  public void run()
  {
    try
    {
      /*
       * Create a sorted list of numerical concept IDs.
       */
      final int[] conceptIds = new int[graph.getConcepts().size()];
      int idx = 0;
      for (ONDEXConcept c : graph.getConcepts()) {
        conceptIds[idx++] = c.getId();
      }
      Arrays.sort(conceptIds);
      
      //Header line:
      output.append("ID").append(SEPARATOR).append("PID").append(SEPARATOR)
          .append("Concept type").append(NEW_LINE);
      
      for (int id : conceptIds) {
        ONDEXConcept c = graph.getConcept(id);
        output.append(String.valueOf(c.getId())).append(SEPARATOR);
        output.append(c.getPID()).append(SEPARATOR);
        output.append(c.getOfType().getId());
        output.append(NEW_LINE);
      }

    }
    catch(Exception e) {
      throw new RuntimeException("Failed to create concept list", e);
    }
  }
}
